################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../lnk_msp430g2231.cmd 

C_SRCS += \
../LocalPositionSystem.c \
../TaskManager.c \
../UARTInterface.c \
../main.c \
../softserial.c 

OBJS += \
./LocalPositionSystem.obj \
./TaskManager.obj \
./UARTInterface.obj \
./main.obj \
./softserial.obj 

C_DEPS += \
./LocalPositionSystem.pp \
./TaskManager.pp \
./UARTInterface.pp \
./main.pp \
./softserial.pp 

C_DEPS__QUOTED += \
"LocalPositionSystem.pp" \
"TaskManager.pp" \
"UARTInterface.pp" \
"main.pp" \
"softserial.pp" 

OBJS__QUOTED += \
"LocalPositionSystem.obj" \
"TaskManager.obj" \
"UARTInterface.obj" \
"main.obj" \
"softserial.obj" 

C_SRCS__QUOTED += \
"../LocalPositionSystem.c" \
"../TaskManager.c" \
"../UARTInterface.c" \
"../main.c" \
"../softserial.c" 


