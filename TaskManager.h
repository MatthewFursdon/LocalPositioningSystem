/*
 * TaskManager.h
 *
 *  Created on: Mar 7, 2016
 *      Author: PC
 */

#ifndef TASKMANAGER_H_
#define TASKMANAGER_H_

#include "task.h"
#include "msp430.h"

void ConfigureTimer();
void TaskLoop(Task* Tasks);
uint16_t MSTime;



#endif /* TASKMANAGER_H_ */
