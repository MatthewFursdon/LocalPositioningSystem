/*
 * UARTInterface.h
 *
 *  Created on: Mar 7, 2016
 *      Author: PC
 */

#ifndef UARTINTERFACE_H_
#define UARTINTERFACE_H_

#include <msp430.h>
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include "softserial.h"
#include "AccessPoint.h"

//
// code commented out because MSP430G2231 does not have a hardware UART
//

/*
struct baud_value
{
    uint32_t baud;
    uint16_t UCAxBR0;
    uint16_t UCAxBR1;
    uint16_t UCAxMCTL;
};

const static struct baud_value BaudRateTable[] = {{(uint32_t)9600,(uint16_t)104,(uint16_t)0,(uint16_t)0x2}};


enum BAUDRATE{BAUDRATE_9600=(short)0};
*/
void SetupClock();
void SetupUART(short RATE);
void WriteToUart(const char *s);



/*
void BlockingTransmit(char* string);
void NonBlockingTransmit(char* string);



inline bool CanRxData()
{
return (true);
}

inline bool CanTxData()
{
return(true);
}
*/


void WriteAPDataToUart(AccessPoint* P);

#endif /* UARTINTERFACE_H_ */
