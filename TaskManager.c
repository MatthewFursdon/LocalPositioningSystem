/*
 * TaskManager.c
 *
 *  Created on: Mar 7, 2016
 *      Author: PC
 */

#include "TaskManager.h"


// OVERWRITTEN BY SOFTSERIAL
/*
void ConfigureTimer()
{
	  TACCTL0 |= CCIE;	//Enable Interrupts on Timer
	    TACCR0 = 12500;	//Number of cycles in the timer
	    TACTL |= TASSEL_2;	//Use ACLK as source for timer
	    TACTL |= MC_1;	//Use UP mode timer
	    TACTL |= ID_3;
	    _bis_SR_register(GIE);
}



// OLD TIMER INTERUPPT
#pragma vector=TIMERA0_VECTOR
__interrupt void Timer_A (void)
{
  // P1OUT ^= BIT0;                          // Toggle P1.0
	MSTime+= 100;
}
*/


void TaskLoop(Task* Tasks)
{

	//simple periodic task manager
	//high speed stuff handled by ISR's
	// data proccessed in task.

	//const int taskCount = sizeof(Tasks) / sizeof(Tasks[0]);

	while(1)
	{
		uint8_t  i = 0;

		while(i < 4)
		{
			if(Tasks[i].Period != 0)
			{
				if(MSTime > Tasks[i].next)
				{
					(*Tasks[i].Function)();
					Tasks[i].next = MSTime + Tasks[i].Period;
				}

			}
			i++;
		}

	}

}
