
/*
 * LPS.h
 *
 *  Created on: Mar 12, 2016
 *      Author: PC
 */

#ifndef LOCALPOSITIONSYSTEM_H_
#define LOCALPOSITIONSYSTEM_H_

#define SERVERAP "2degrees Broadband - EA8B"
#define SERVERAPPASS "removed"


#include <stdint.h>
//#include <stdbool.h>
#include "AccessPoint.h"
#include "UartInterface.h"
//#include "stdlib.h"

AccessPoint AccessPoints[2];
uint8_t NumberOfAccessPoints;
//bool ApDataTransmitted;

void ProccessAccessPointString(char c[]);
void ClearAccessPointList();
void RequestAccessPointList();
void InitWirelessUnit();
void Task_ReadUARTOutput();
void Task_TransmitAPList();
int8_t ExtractSigStrength(char *str);
int8_t ExtractAPID(char *str);
void StoreAPInfo(int8_t APID,int8_t SS);
void StreamAPData();

//ESP functions
inline void ConnectToWirelessNetwork(char* SSID,char* Password);
inline void ResetESP();
inline void SetUpESP();
inline void ConnectToServer();
inline void StartTransmission();
inline void EndTransmission();
inline void SendTCPIPData(char* data);

#endif /* ACCESSPOINT_H_ */
