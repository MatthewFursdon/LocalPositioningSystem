/*
 * LocalPositionSystem.c
 *
 *  Created on: Mar 12, 2016
 *      Author: PC
 */


#include "LocalPositionSystem.h"


/*
void RequestAccessPointList()
{
	NumberOfAccessPoints = 0;
	WriteToUart("AT+CWLAP\r\n");
}
*/

void StoreAPInfo(int8_t APID,int8_t SS)
{
	uint8_t i = 0;
	uint8_t updated = 0;
	while(i < NumberOfAccessPoints)
	{
		if(AccessPoints[i].APID == APID)
		{
			AccessPoints[i].SignalStrength = SS;
			updated = 1;
		}
		i++;
	}

	if(!updated)
	{
		AccessPoint NewAP;
		NewAP.APID = APID;
		NewAP.SignalStrength = SS;
		AccessPoints[i] = NewAP;
		NumberOfAccessPoints = i;
	}
}

void StreamAPData()
{
 uint8_t j = 0;
 // send 'em all and let server sort 'em out
 while(j < NumberOfAccessPoints)
 {
	 WriteAPDataToUart(&AccessPoints[j]);
	 j++;
 }
 WriteToUart("\r\n");
}
void WaitForOkay()
{
	while(!OKFlag)
	{
		;
	}
	OKFlag = 0;
}


void Task_TransmitAPList()
{
	ConnectToWirelessNetwork(SERVERAP,SERVERAPPASS);
	WaitForOkay();
	ConnectToServer();
	WaitForOkay();
	StartTransmission();
	StreamAPData();
	EndTransmission();
	WaitForOkay();
}



int8_t ExtractSigStrength(char *str)
{

	//
	// value will always be eith -0-99 or 0-99
	//
	char a = str[0];
	char b = str[1];
	char c = str[2];
	if(a != '-')
	{
		uint8_t v1 = a-'0';
		uint8_t v2 = b-'0';

		return((10*v1)+v2);
	}
	else
	{

		uint8_t v1 = b-'0';
		uint8_t v2 = c-'0';

		return(-((10*v1)+v2));
	}

}

int8_t ExtractAPID(char *str)
{
	// Only get the ID of AP's that are part of the LPS system.
	if(str[1] == 'L')
	{

		uint8_t a = str[2]-'0';
		uint8_t b = str[3]-'0';
		uint8_t c = str[4]-'0';

		return(100*a + 10*b + c);

	}
	return -1;
}


char StoredString[6];
uint8_t spos = 0;
int8_t APID;
int8_t sstrength;
uint8_t ModeID = 0;
void Task_RequestAccessPointList()
{

	OKFlag=0;
	NumberOfAccessPoints = 0;
	WriteToUart("AT+CWLAP\r\n");



		while(!OKFlag)
		{


	        int c;
	        if ( !SoftSerial_empty() ) {
	            while((c=SoftSerial_read()) != -1)
	            {

	            	if(c == '(' || c == ')')
	            	{
	            		spos = 0;
	            		ModeID = 0;
	            	}

	            	if(c == ',')
	            	{
	            		if(ModeID == 0)
	            		{
	            			//ignore
	            		}
	            		else if(ModeID == 1)
	            		{
	            			APID = ExtractAPID(&StoredString[0]);
	            		}
	            		else if(ModeID == 2)
	            		{
	            			sstrength = ExtractSigStrength(&StoredString[0]);
	            			StoreAPInfo(APID,sstrength);
	            		}
	            		else
	            		{
	            			// ignore
	            		}
	            		spos = 0;
	            		ModeID++;
	            	}
	            	else
	            	{
	            		StoredString[spos] = c;
	            		spos++;
	            	}

	            }
	        }

		}
		OKFlag = 0;
}

void InitWirelessUnit()
{
	//gives 'ready' not 'OK' as a response, so just wait manually
	WriteToUart("AT+RST\r\n");
	__delay_cycles(80000000);
	// response depends on existing CWMODE
	 WriteToUart("AT+CWMODE=3\r\n");
     __delay_cycles(80000000);
     WriteToUart("AT+CIPMODE=1\r\n");
     WaitForOkay();

}


void WriteAPDataToUart(AccessPoint* P)
{

        SoftSerial_xmit('+');
        SoftSerial_xmit((unsigned char)P->APID);
        SoftSerial_xmit((unsigned char)P->SignalStrength);
        SoftSerial_xmit('-');

}

void ConnectToWirelessNetwork(char* SSID,char* Password)
{

	// format: AT+CWJAP="SSID","Password"\r\n
	WriteToUart("AT+CWJAP=\"");
	WriteToUart(SSID);
	WriteToUart("\",\"");
	WriteToUart(Password);
	WriteToUart("\"\r\n");

}


inline void ResetESP()
{
	WriteToUart("AP+RST\r\n");
}

inline void SetUpESP()
{

}

inline void ConnectToServer()
{
	// hardcoded for now, will change later
	WriteToUart("AT+CIPSTART=\"TCP\",\"192.168.178.35\",4444\r\n");
}


inline void StartTransmission()
{
	WriteToUart("AT+CIPSEND\r\n");
}

inline void EndTransmission()
{
	WriteToUart("\r\n+++\r\n"); // break out of transmission mode by sending +++ packet
	WriteToUart("AT+CIPCLOSE\r\n"); // close conection
}

inline void SendTCPIPData(char* data)
{
	// this implementation will probably change later
	WriteToUart(data);
}
