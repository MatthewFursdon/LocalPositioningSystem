/*
 * Task.h
 *
 *  Created on: Mar 7, 2016
 *      Author: PC
 */

#ifndef TASK_H_
#define TASK_H_


#include <stdint.h>
typedef struct
{

	void (*Function)(void);
	uint16_t Period; //in ms
	uint16_t next;

} Task;

#endif /* TASK_H_ */
