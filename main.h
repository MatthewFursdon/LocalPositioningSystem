/*
 * main.h
 *
 *  Created on: Mar 7, 2016
 *      Author: PC
 */

#ifndef MAIN_H_
#define MAIN_H_
#include "UARTInterface.h"
#include "TaskManager.h"
#include "config.h"
#include "softserial.h"
#include "LocalPositionSystem.h"

void SetupGPIO();
void Task1();
void Task2();
void Task_RequestAccessPointList();

#endif /* MAIN_H_ */
