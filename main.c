
#include "main.h"
/*
 * main.c
 */




int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer

	SetupGPIO();
	//SetupUART(BAUDRATE_9600);
	//ConfigureTimer();
	SetupClock();
	Task Tasks[] = {{Task1,(uint16_t)1000,0},
					{Task2,(uint16_t)1000,0},
					//{Task_ReadUARTOutput,(uint16_t)100,0},
					{Task_RequestAccessPointList,(uint16_t)10000,0},
					{Task_TransmitAPList,(uint16_t)20000,20000},
	};

    SoftSerial_xmit((uint8_t)'\n');

    InitWirelessUnit();
	TaskLoop(Tasks);





}

void PetWatchdog()
{


}

void SetupGPIO()
{
	P1DIR |=BIT0;
	P1DIR |=BIT6;
	//P1DIR |= ;
}


void Task1()
{
	P1OUT ^= BIT0;
}

void Task2()
{
	P1OUT ^= BIT6;
}

/*
void Task_RequestAccessPointList()
{
	RequestAccessPointList();
}
*/

void SetupClock()
{

	 WDTCTL = WDTPW + WDTHOLD;       // Stop watchdog timer, can comment out if -mdisable-watchdog

	    /**
	     * Setting these flags allows you to easily measure the actual SMCLK and ACLK
	     * frequencies using a multimeter or oscilloscope.  You should verify that SMCLK
	     * and your desired F_CPU are the same.
	     */

	    P1DIR |= BIT0; P1SEL |= BIT0;   // measure P1.0 for actual ACLK
	    P1DIR |= BIT4; P1SEL |= BIT4;   // measure P1.4 for actual SMCLK

	    // my XTAL is closer to 32.768 with this setting.
	    // It might not be work as well for you.
	    BCSCTL3 = (BCSCTL3 & ~XCAP_3) | XCAP_0;

	    __delay_cycles(0xffff);         // let XTAL stabilize

	#if defined(CALIBRATE_DCO)
	    Set_DCO(F_CPU/4096);            // Calibrate and set DCO clock to F_CPU define
	#else
	    #if 1
	        DCOCTL = 0x00;              // Set DCOCLK to 16MHz using values obtained by
	        BCSCTL1= 0x8F;              // calibrating a chip. Values may be different
	        DCOCTL = 0x7E;              // for every chip and temperature
	    #else
	        DCOCTL = 0x00;                  // Set DCOCLK to precalibrated 1MHz
	        BCSCTL1 = CALBC1_1MHZ;
	        DCOCTL = CALDCO_1MHZ;
	    #endif
	#endif

	    SoftSerial_init();              // Configure TIMERA and RX/TX pins
	    __enable_interrupt();           // let the TIMERA do its work


}

