/*
 * AccessPoint.h
 *
 *  Created on: Mar 12, 2016
 *      Author: PC
 */

#ifndef ACCESSPOINT_H_
#define ACCESSPOINT_H_

#include <stdint.h>
typedef struct
{
	uint8_t APID;
	int8_t SignalStrength;

} AccessPoint;


#endif /* ACCESSPOINT_H_ */
